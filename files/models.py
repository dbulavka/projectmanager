from django.db import models

# Create your models here.
class File(models.Model):
	owner = models.CharField(max_length=200)
	docfile = models.FileField(upload_to='projects/')