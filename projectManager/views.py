from django.shortcuts import render
from projects.models import Project
from news.models import New
from videos.models import Video
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.mail import send_mail

def home(request):
	projectsList = Project.objects.all().order_by('-pub_date')[:4]
	newsList = New.objects.all().order_by('-pub_date')[:4]
	videosList = Video.objects.all().order_by('-pub_date')[:4]
	context = {'projectsList': projectsList,'newsList':newsList,'videosList':videosList}
	return render(request, 'projectManager/home.html', context)

def contact(request):
	if request.method == 'GET':
		return render(request, 'projectManager/contact.html')
	else:
		asunto = request.POST.get('asunto_txt')
		mensaje = request.POST.get('mensaje_txa')
		nombre = request.POST.get('nombre_txt')
		telefono = request.POST.get('telefono_txt')
		email = request.PoST.get('email_txt')

		send_mail(asunto, mensaje, email,
    	['info@imasdottokrause.com.ar'], fail_silently=False)
		return HttpResponseRedirect(reverse('home'))

def about(request):
	return render(request, 'projectManager/about.html')

def send_contact(request):
	return HttpResponseRedirect(reverse('home'))

def login_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			login(request, user)
			return HttpResponseRedirect(reverse('home'))
		else:
			return HttpResponseRedirect(reverse('home'))
	else:
		return HttpResponseRedirect(reverse('home'))

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))