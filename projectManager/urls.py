from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projectManager.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'projectManager.views.home', name='home'),
    url(r'^logout/', 'projectManager.views.logout_view', name='logout'),
    url(r'^login/', 'projectManager.views.login_view', name='login'),
    url(r'^contact/', 'projectManager.views.contact', name='contact'),
    url(r'^send_contact/', 'projectManager.views.send_contact', name='send_contact'),
    url(r'^about/', 'projectManager.views.about', name='about'),
    url(r'^news/',include('news.urls', namespace='news')),
    url(r'^videos/',include('videos.urls', namespace='videos')),
    url(r'^projects/',include('projects.urls', namespace='projects')),
    url(r'^admin/', include(admin.site.urls)),
)
