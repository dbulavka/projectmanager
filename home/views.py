from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from projects.models import Project

def index(request):
	return render(request,'home/index.html',{'projectList':Project.objects.order_by('timestamp')[:4]})
    
