from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'projectManager.views.home', name='home'),
    # url(r'^projectManager/', include('projectManager.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', include('home.urls', namespace="home")),
    url(r'^projects/', include('projects.urls', namespace="projects")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^upload/', include('files.urls')),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
