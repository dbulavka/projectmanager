from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from videos.models import Video

class Index (ListView):
	template_name = 'videos/index.html'
	context_object_name = 'videosList'
	def get_queryset(self):
		return Video.objects.order_by('-pub_date')

class Detail(DetailView):
	model = Video
	template_name = 'videos/detail.html'

class Add(CreateView):
	model = Video
	success_url = reverse_lazy('videos:index')

class Update(UpdateView):
	model = Video
	field = ['title','description']

class Delete(DeleteView):
	model = Video
	success_url = reverse_lazy('videos:index')
