from django.db import models

class Video(models.Model):
	title = models.CharField(max_length=100)
	description = models.TextField()
	portrait = models.CharField(max_length=250)
	pub_date = models.DateTimeField('date published')
	embedded = models.CharField(max_length=250)

	def __unicode__(self):
		return self.title