from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from projects.models import Project

class Index (ListView):
	template_name = 'projects/index.html'
	context_object_name = 'projectsList'
	def get_queryset(self):
		return Project.objects.order_by('-pub_date')

class Detail(DetailView):
	model = Project
	template_name = 'projects/detail.html'

class Add(CreateView):
	model = Project
	success_url = reverse_lazy('projects:index')

class Update(UpdateView):
	model = Project
	field = ['title','description']

class Delete(DeleteView):
	model = Project
	success_url = reverse_lazy('projects:index')