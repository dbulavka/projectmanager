from django.forms import ModelForm
from projects.models import Project, Picture
from django.forms.models import inlineformset_factory

class ProjectForm(ModelForm):
	class Meta:
		model = Project

#class PictureForm(ModelForm):
#	class Meta:
#		model = Picture
#		exclude = ('project',)

#PictureFormSet = inlineformset_factory(Project, Picture)