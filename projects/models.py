from django.db import models

# Create your models here.
class Project(models.Model):
	title = models.CharField(max_length=100)
	description = models.TextField()
	portrait = models.CharField(max_length=250)
	repository = models.CharField(max_length=250)
	pub_date = models.DateTimeField('date published')

	def __unicode__(self):
		return self.title

# class Picture(models.Model):
# 	project = models.ForeignKey(Project)
# 	title = models.CharField(max_length=50)
# 	file  = models.FileField(upload_to='projects')