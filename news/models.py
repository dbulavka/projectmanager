from django.db import models

class New(models.Model):
	title = models.CharField(max_length=100)
	portrait = models.CharField(max_length=250)
	description = models.TextField()
	pub_date = models.DateTimeField('date published')

	def __unicode__(self):
		return self.title