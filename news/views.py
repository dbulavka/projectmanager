from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from news.models import New

class Index (ListView):
	template_name = 'news/index.html'
	context_object_name = 'newsList'
	def get_queryset(self):
		return New.objects.order_by('-pub_date')

class Detail(DetailView):
	model = New
	template_name = 'news/detail.html'

class Add(CreateView):
	model = New
	success_url = reverse_lazy('news:index')

class Update(UpdateView):
	model = New
	field = ['title','description']

class Delete(DeleteView):
	model = New
	success_url = reverse_lazy('news:index')
