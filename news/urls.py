from django.conf.urls import patterns, url
from news import views

urlpatterns = patterns('',
	url(r'^$',views.Index.as_view(),name='index'),
	url(r'^add/$',views.Add.as_view(),name='add'),
	url(r'^(?P<pk>\d+)/update/$',views.Update.as_view(),name='update'),
	url(r'^(?P<pk>\d+)/delete/$',views.Delete.as_view(),name='delete'),
	url(r'^(?P<pk>\d+)/$',views.Detail.as_view(),name='detail'),
)